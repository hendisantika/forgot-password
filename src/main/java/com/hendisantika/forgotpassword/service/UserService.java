package com.hendisantika.forgotpassword.service;

import com.hendisantika.forgotpassword.dto.UserRegistrationDto;
import com.hendisantika.forgotpassword.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by IntelliJ IDEA.
 * Project : forgot-password
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/04/18
 * Time: 19.16
 * To change this template use File | Settings | File Templates.
 */
public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);

    void updatePassword(String password, Long userId);
}