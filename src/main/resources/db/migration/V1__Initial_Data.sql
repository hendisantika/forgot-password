CREATE TABLE user (
	id BIGINT NOT NULL
	,email VARCHAR(255)
	,first_name VARCHAR(255)
	,last_name VARCHAR(255)
	,password VARCHAR(255)
	,PRIMARY KEY (id)
	) engine = MyISAM;

CREATE TABLE role (
	id BIGINT NOT NULL
	,name VARCHAR(255)
	,PRIMARY KEY (id)
	) engine = MyISAM;

CREATE TABLE users_roles (
	user_id BIGINT NOT NULL
	,role_id BIGINT NOT NULL
	) engine = MyISAM;

CREATE TABLE password_reset_token (
	id BIGINT NOT NULL
	,expiry_date DATETIME NOT NULL
	,token VARCHAR(255) NOT NULL
	,user_id BIGINT NOT NULL
	,PRIMARY KEY (id)
	) engine = MyISAM;


create table hibernate_sequence (
   next_val bigint
) engine=MyISAM;



ALTER TABLE password_reset_token ADD CONSTRAINT UK_g0guo4k8krgpwuagos61oc06j UNIQUE (token);

ALTER TABLE user ADD CONSTRAINT UKob8kqyqqgmefl0aco34akdtpe UNIQUE (email);

ALTER TABLE password_reset_token ADD CONSTRAINT FK5lwtbncug84d4ero33v3cfxvl FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE users_roles ADD CONSTRAINT FKt4v0rrweyk393bdgt107vdx0x FOREIGN KEY (role_id) REFERENCES ROLE (id);

ALTER TABLE users_roles ADD CONSTRAINT FKgd3iendaoyh04b95ykqise6qh FOREIGN KEY (user_id) REFERENCES user (id);

-- Dump data of "role" -------------------------------------
INSERT INTO `role`(`id`,`name`) VALUES ( '1', 'ROLE_ADMIN' );
INSERT INTO `role`(`id`,`name`) VALUES ( '2', 'ROLE_USER' );
-- ---------------------------------------------------------

-- Dump data of "user" -------------------------------------
INSERT INTO `user`(`id`,`email`,`first_name`,`last_name`,`password`) VALUES
( '1', 'hendisantika@yahoo.co.id', 'Hendi', 'Santika', '$2a$10$/dktAoUPlSEvNcgqyhCO5O5d8UtIo2V2L0PlfVyfCwacRtGw.38G2' ),
( '2', 'hendisantika@gmail.com', 'Hendi', 'Santika', '$2a$10$/dktAoUPlSEvNcgqyhCO5O5d8UtIo2V2L0PlfVyfCwacRtGw.38G3' );
-- ---------------------------------------------------------
-- hendisantika@yahoo.co.id / admin123
-- Dump data of "users_roles" ------------------------------
INSERT INTO `users_roles`(`user_id`,`role_id`) VALUES ( '1', '1' );
INSERT INTO `users_roles`(`user_id`,`role_id`) VALUES ( '2', '2' );
-- ---------------------------------------------------------

insert into hibernate_sequence values ( 1 );

insert into hibernate_sequence values ( 2 );
