# Forgot Password

Run this program by this command :

`mvn clean spring-boot:run`

Invalid Login Page

![Invalid Login Page](img/invalid.png "Invalid Login Page")

Forgot Password Page

![Forgot Password Page](img/forgot.png "Forgot Password Page")

Send Email Page

![Send Email Page](img/sent.png "Send Email Page")


Get Email Page

![Get Email Page](img/email.png "Get Email Page")

Reset Password Page

![Reset Password Page](img/reset.png "Reset Password Page")

Login Page

![Login Page](img/login.png "Login Page")

Login Successful Page

![Login Successfull Page](img/success.png "Login Successfull Page")


